docker volume create osm-data

time docker run -v /root/kuopio.osm.pbf:/data.osm.pbf -v osm-data:/var/lib/postgresql/14/main overv/openstreetmap-tile-server:2.2.0 import

docker run -p 8080:80 -v osm-data:/var/lib/postgresql/14/main -d overv/openstreetmap-tile-server:2.2.0 run