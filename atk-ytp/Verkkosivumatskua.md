# ATK-YTP Kuopio 2022

### **Sanoiko joku ATK-YTP?**

AaTeeKoo-Yhteistoimintapäivät, tai tutummin ATK-YTP vuosimallia 2022 järjestetään savon syvämessä eli Kuopijossa 19.-21.10.2022. ATK-YTP on Tietotekniikan opiskelijoiden liiton, TiTOL:n vuosittainen Suomen IT-alan opiskelijoiden seminaaritapahtuma ja tänä vuonna vetovastuun ottaa isäntäkaupungin tietojenkäsittelytieteen ainejärjestö Serveri ry.

Tällein lyhkäisesti tapahtuma pähkinänkuoressa, päivisin kuunnellaan alan rautaisten ammattilaisten luentoja kampuksella SN100 salissa ja iltaisin sitten verkostoidutaan muiden kanssaopiskelijoiden kanssa ja tutustutaan isäntäkaupunkiin mm. rastikierroksen merkeissä. Näiden lisäksi TiTOL järjestää tapahtuman ohessa myös syyskokouksensa ja tapahtuman toisena aamuna tapahtuman pääsponsori Efecte järjestää aamusaunan!

### **Aikataulu**

<kalenteri>

### **Majoitus**

Muiden kaupunkien immeiset majoitetaan Kuopio-hallin vieressä sijaitseviin Hatsalan, sekä Klassikan kouluille Opistotien varressa. Majoituksessa oleillessa pidä huoli, että muut saavat nukkumarauhan, sekä pidetään paikat puhtaina, jotta tila säilyy viihtyisänä!

Majoitus avataan keskiviikkoaamuna heti 8.00 ja se sulkeutuu 10.00. Iltaisin majoitukset aukeavat klo 21.00 alkaen. Torstaina majoitus suljetaan klo 11.00 ja se avataan uudestaan 21.00. Perjantaina on aika lyödä kimpsut ja kampsut läjään ja poistua kotia kohti, viimeistään klo 11.00.

Matkatavaroita voi ottaa matkaan oman maun mukaan, mutta vahvasti suosittelemme nappamaan kotoa matkaan nukkumatarvikkeet, joilla voi nukkua vähän kovemmallakin lattialla, pyyhkeen ja lämpimästi päälle!

Majoituksissa huoneita on runsaasti, ja ne ovat jaettu ainejärjestöittäin. Tilat lukitaan aina päivän ajaksi, joten sinne voi jättää henkilökohtaisia tavaroita talteen, joita kokee ettei tarvitse päivän aikana.

### **Ai nälkä vai?**

Kampuksellamme on monia eri kouluruokaloita ja heti välittömässä läheisyydessä meidän pääluentosaliamme SN100:sta on [Snellmanian oma ruokala](https://www.foodandco.fi/ravintolat/Ravintolat-kaupungeittain/kuopio/ita-suomen-yliopisto--snellmania). Vaihtoehtoisesti Savilahdentien toiselta puolelta löytyy [Tietoteknian ruokala](https://www.foodandco.fi/ravintolat/Ravintolat-kaupungeittain/kuopio/tietoteknia/). Vilauttamalla opiskelijakorttiasi, saat näistä paikoista opiskelijahintaista ruokaa.

Jos kauppatarve tulee, niin heti kampukselta poistuttuasi tien toiselta puolelta löytyy Prisma, joka kätkee sisäänsä myös muita palveluita ja ravintoloita (ml. A-marketin, tai tuttavallisemmin Alkon, mistä voi tarvittaessa täydentää G(ambina)-, J(allu)-, sekä V(älivesi)-vitamiini varastoja).

### **Tapahtumia**

##### **Muailmannavan Maratooni/Kalakukon Kierto**

Kunhan ensimmäisen päivän luennoilla on ensin tankattu pää täyteen (informaatiota) niin mitteepä muutakaan sitä tekisi, kun lähtisi tuttuun tapaan kiertämään rastikierrosta. Muailmannavan Maratoonissa pääsette tutustumaan Kuopion keskustaan ja sitä ympäröiviin alueisiin meidän paikallisten opiskelijoiden pitämien rastien johdolla. Kasaa max. 8 hengen joukkue ja testatkaa fyysistä kestävyyttänne. Onko juuri teistä Muailmannavan valloittajiksi?

##### **IT-taaperrus/Virmavipellys/Koodikaahaus**

**taapertaa**

1. kävellä hitaasti ja horjuen

Vaikka askel saattaa tuntua raskaalta edellisen päivän urotekojen jälkeen, on aika lähteä haastamaan toisen päivän rastikierroksessa yhteistyökumppaneiden ja paikallisten firmojen pitämiä rasteja. Lähtekää yytsimään max. 8 IT-weljen kera, mitä nämä alan konkarit ovat keksineet päänne menoksi.

##### **TINT & TILT, tinttaus ja tilttaus feat. Petri Nygård**

Satoipa vettä tahi ei, ekan päivän päätteeksi tulee olemaan mä-mä-mä-mä-märkää, sillä lavalta kajahtaa Petri Nygårdin riimit Ravintola Apteekkarissa järjestettävissä jatkobileissä. Täällä bileet löytyvät kahdesta eri kerroksesta, alakerta tarjoilee istumapaikkoja karaoken kera niille, keillä meinaa veto loppua, toisesta kerroksesta löytyy sitten isompi tanssilattia missä kovemman kisakunnon omaavat yksilöt voivat kuluttaa kengänpohjia tanssilattialla aina yön pikku tunneille asti.

##### **ReivIT**

Toisena iltana juhlakansa kärrätään Calacuccoon, jossa DJ:t pitävät ReivITasanteen kuumana. Rauhallisemmille menijöille löytyy aina yhtä viihtyisä karaoke puoli, jossa voit vaikka testata kuinka "Lentävä kalakukko" lävähtää ilmoille.

##### **Efecten Aamukylypy**

Tapahtuman toisena aamuna ennen luentojen alkamista tapahtuman pääsponsori Efecte järjestää aamusaunan. Aamukylvyn, sekä pienen purtavan kera toivottavasti pahinkin ruoste edellispäivän jäljiltä saadaan poistettua ja uusi päivä startattua mahdollisimman tehokkaasti. Paikkoja tähän kehon ja mielen resetointiin on rajoitettu määrä, joten kannattaa olla skarppina myöhemmin julkaistavan ilmoittautumisen kanssa!

### **Kaupungissa liikkuminen**

Vaikka lonkkavolvo on aina vaihtoehto, niin välillä on kätevämpi hyödyntää kaupunkimme muita liikkumistapoja, kuten kaupunkipyöriä (Freebike), paikallisliikenteen bussit (Vilkku- sovellus tai [nettisivut](https://vilkku.kuopio.fi/)), sekä sähköpotkulautoja (Bird ja TIER). Jos halajaa juusto katolla matkustaa niin kaupungista löytyy hyvä läjä taksejakin joka lähtöön ([VieVie](https://www.vievie.fi/)/[Taksiykköset](https://taksi1.fi/)/[Menevä](https://meneva.fi/fi)).

### **Turvallisemman tilan periaatteet**

Jotta varmistuisimme siitä, että tapahtuma on mahdollisimman onnistunut kaikin puolin, haluamme, että tapahtumassa noudatetaan turvallisemman tilan periaatteita, joihin voitte tutustua joko yläpalkista löytyvän ”Safe space” napin alta, tai vaihtoehtoisesti klikkaamalla [tästä](https://atk-ytp.org/safespace).

### **Yhteistyössä (Logot tähän, Efecte ja Innofactor ylimpinä).**

Efecte Oyj\
Innofactor Oyj\
TEK ry\
TIVIA ry\
Talentree Oy\
Huld Oy\
Ikius Oy\
Adamant Health Oy

##### **Yhteystiedot:** (Vaikka kuva linkkeinä, tg=ytp ryhmä, säpö=atk-ytp@serveriry.fi, fb=serveriry, ig=serveriry)

TG/Säpö/Facebook/IG

Serverin logo tms