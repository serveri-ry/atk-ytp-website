// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  ssr: true,
  css: [
    //'@/assets/css/normalize.css',
    // SCSS file in the project
    '@/assets/scss/main.scss'
  ],
  // nitro: {
  //   prerender: {
  //     routes: ['/index', '/calendar', '/404', '/200']
  //   }
  // }
  nitro: {
    preset: 'node-server',
  }
  // title: 'ATK-YTP 2022',
  // meta: [
  //   { charset: 'utf-8' },
  //   { name: 'viewport', content: 'width=device-width, initial-scale=1' },
  //   {
  //     hid: 'description',
  //     name: 'description',
  //     content: 'ATK-YTP järjestetään Kuopiossa 19.10.-21.10.2022. Luvassa huikeita luentoja sekä ikimuistoisia rastikierroksia!'
  //   }
  // ],
})
