module.exports = {
    apps: [
      {
        name: 'ATK-YTP',
        exec_mode: 'cluster',
        instances: 'max',
        script: './.output/server/index.mjs',
        env: {
            "NITRO_PORT": 3535,
        }
      }
    ]
  }